package org.ws.facade;

import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;

import org.ws.listavehiculos.GestionDeTramiteInformacionVehiculosServiceImpl;
import org.ws.listavehiculos.InformacionVehiculosPt;
import org.ws.listavehiculos.requestresponse.ConsultarClaseInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarClaseInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarLineaInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarLineaInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarMarcaInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarMarcaInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarModalidadTransporteInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarModalidadTransporteInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarModeloInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarModeloInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.GenericItemType;

public class ListasVehiculosFacade {
    public ListasVehiculosFacade() {
        super();
    }
    
    public static String getListasParaVehiculo(List<SelectItem> listaClase,
                                             List<SelectItem> listaLinea,
                                             List<SelectItem> listaMarca,
                                             List<SelectItem> listaModalidad,
                                             List<SelectItem> listaModelo){
        
        GestionDeTramiteInformacionVehiculosServiceImpl gestionDeTramiteInformacionVehiculosServiceImpl =
            new GestionDeTramiteInformacionVehiculosServiceImpl();
            InformacionVehiculosPt informacionVehiculosPt =
            gestionDeTramiteInformacionVehiculosServiceImpl.getGestionDeTramiteInformacionVehiculosPortImpl();
        // Add your code to call the desired methods.
        ConsultarClaseInformacionVehiculoMsgResp respClase =  informacionVehiculosPt.consultarClaseInformacionVehiculosOp(new ConsultarClaseInformacionVehiculoMsgReq());
        ConsultarLineaInformacionVehiculoMsgResp respLinea =  informacionVehiculosPt.consultarLineaInformacionVehiculosOp(new ConsultarLineaInformacionVehiculoMsgReq());
        ConsultarMarcaInformacionVehiculoMsgResp respMarca =  informacionVehiculosPt.consultarMarcaInformacionVehiculosOp(new ConsultarMarcaInformacionVehiculoMsgReq());
        ConsultarModalidadTransporteInformacionVehiculoMsgResp respModalidad =  informacionVehiculosPt.consultarModalidadTransporteInformacionVehiculosOp(new ConsultarModalidadTransporteInformacionVehiculoMsgReq());
        ConsultarModeloInformacionVehiculoMsgResp respModelo = informacionVehiculosPt.consultarModeloInformacionVehiculosOp(new ConsultarModeloInformacionVehiculoMsgReq());
        
        
        Iterator<GenericItemType> it1 = respClase.getClaseVehiculo().iterator();
        
        while(it1.hasNext()){
            GenericItemType clase = it1.next();
            listaClase.add(new SelectItem(clase.getCodigo(),clase.getDescripcion()));
        }
        
        
        it1 = respLinea.getLineaVehiculo().iterator();
        while(it1.hasNext()){
            GenericItemType linea = it1.next();
            listaLinea.add(new SelectItem(linea.getCodigo(),linea.getDescripcion()));
        }
        

        
        it1 = respMarca.getMarcaVehiculo().iterator();
        while(it1.hasNext()){
            GenericItemType marca = it1.next();
            listaMarca.add(new SelectItem(marca.getCodigo(),marca.getDescripcion()));
        }
        
        
        it1 = respModalidad.getModalidadTransporte().iterator();
        while(it1.hasNext()){
            GenericItemType modalidad = it1.next();
            listaModalidad.add(new SelectItem(modalidad.getCodigo(),modalidad.getDescripcion()));
        }
        
        
        it1 = respModelo.getModeloVehiculo().iterator();
        while(it1.hasNext()){
            GenericItemType modelo = it1.next();
            listaModelo.add(new SelectItem(modelo.getCodigo(),modelo.getDescripcion()));
        }
        
        
        if(!listaClase.isEmpty() && !listaLinea.isEmpty() && !listaMarca.isEmpty() && !listaModalidad.isEmpty() && !listaModelo.isEmpty()){
            return "Se han cargado las listas correctamente";
        }else{
            return "Alguna lista no se ha cargado correctamente";
        }
        
    }
}
