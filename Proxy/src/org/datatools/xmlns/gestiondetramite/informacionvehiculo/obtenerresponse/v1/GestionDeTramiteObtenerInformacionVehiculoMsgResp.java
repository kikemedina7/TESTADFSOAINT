
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vehiculo" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}VehiculoType" minOccurs="0"/&gt;
 *         &lt;element name="ListaPropietario" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}ListaPropietarioType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "vehiculo", "listaPropietario" })
@XmlRootElement(name = "GestionDeTramite_ObtenerInformacionVehiculoMsgResp")
public class GestionDeTramiteObtenerInformacionVehiculoMsgResp {

    @XmlElement(name = "Vehiculo")
    protected VehiculoType vehiculo;
    @XmlElement(name = "ListaPropietario")
    protected ListaPropietarioType listaPropietario;

    /**
     * Gets the value of the vehiculo property.
     *
     * @return
     *     possible object is
     *     {@link VehiculoType }
     *
     */
    public VehiculoType getVehiculo() {
        return vehiculo;
    }

    /**
     * Sets the value of the vehiculo property.
     *
     * @param value
     *     allowed object is
     *     {@link VehiculoType }
     *
     */
    public void setVehiculo(VehiculoType value) {
        this.vehiculo = value;
    }

    /**
     * Gets the value of the listaPropietario property.
     *
     * @return
     *     possible object is
     *     {@link ListaPropietarioType }
     *
     */
    public ListaPropietarioType getListaPropietario() {
        return listaPropietario;
    }

    /**
     * Sets the value of the listaPropietario property.
     *
     * @param value
     *     allowed object is
     *     {@link ListaPropietarioType }
     *
     */
    public void setListaPropietario(ListaPropietarioType value) {
        this.listaPropietario = value;
    }

}
