package com.librerias;

public class LoggerUtil {
    public LoggerUtil() {
        super();
    }
    
    public static void sendLogMessage(String typeLog,String msg){
        System.out.println("**** Estado del "+typeLog+"  *****");
        System.out.println("\n    "+msg+"     \n");
        System.out.println("*******************************");
    }
}
