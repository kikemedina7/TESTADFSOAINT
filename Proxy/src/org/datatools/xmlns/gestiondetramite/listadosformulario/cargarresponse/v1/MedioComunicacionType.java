
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MedioComunicacionType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="MedioComunicacionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMedio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreMedio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedioComunicacionType", propOrder = { "idMedio", "nombreMedio" })
public class MedioComunicacionType {

    protected String idMedio;
    protected String nombreMedio;

    /**
     * Gets the value of the idMedio property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdMedio() {
        return idMedio;
    }

    /**
     * Sets the value of the idMedio property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdMedio(String value) {
        this.idMedio = value;
    }

    /**
     * Gets the value of the nombreMedio property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNombreMedio() {
        return nombreMedio;
    }

    /**
     * Sets the value of the nombreMedio property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNombreMedio(String value) {
        this.nombreMedio = value;
    }

}
