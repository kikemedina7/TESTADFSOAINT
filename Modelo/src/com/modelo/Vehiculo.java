package com.modelo;

import java.io.Serializable;

public class Vehiculo implements Serializable{
    @SuppressWarnings("compatibility:-5574906888426814008")
    private static final long serialVersionUID = 1L;

    private Integer idVehiculo;
    private String alertasLegales;
    private Integer codigoMarca;
    private String descripcionMarca;
    private Integer codigoClase;
    private String descripcionClase;
    private Integer codigoLinea;
    private String descripcionLinea;
    private Integer codigoModelo;
    private String descripcionModelo;
    private Integer codigoCiudad;
    private String descripcionCiudad;
    private Integer codigoModalidadTransporte;
    private String descripcionModalidadTransporte;  
    private String placa;
    private String color;
    private Boolean robado;

    
    public Vehiculo() {
        super();
    }


    public Vehiculo(Integer idVehiculo, String alertasLegales, Integer codigoMarca, String descripcionMarca,
                    Integer codigoClase, String descripcionClase,  Integer codigoLinea,
                    String descripcionLinea, Integer codigoModelo, String descripcionModelo, Integer codigoCiudad,
                    String descripcionCiudad, Integer codigoModalidadTransporte, String descripcionModalidadTransporte,
                    String placa, String color, Boolean robado) {
        this.idVehiculo = idVehiculo;
        this.alertasLegales = alertasLegales;
        this.codigoMarca = codigoMarca;
        this.descripcionMarca = descripcionMarca;
        this.codigoClase = codigoClase;
        this.descripcionClase = descripcionClase;
        this.codigoLinea = codigoLinea;
        this.descripcionLinea = descripcionLinea;
        this.codigoModelo = codigoModelo;
        this.descripcionModelo = descripcionModelo;
        this.codigoCiudad = codigoCiudad;
        this.descripcionCiudad = descripcionCiudad;
        this.codigoModalidadTransporte = codigoModalidadTransporte;
        this.descripcionModalidadTransporte = descripcionModalidadTransporte;
        this.placa = placa;
        this.color = color;
        this.robado = robado;
    }


    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setAlertasLegales(String alertasLegales) {
        this.alertasLegales = alertasLegales;
    }

    public String getAlertasLegales() {
        return alertasLegales;
    }

    public void setCodigoMarca(Integer codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    public Integer getCodigoMarca() {
        return codigoMarca;
    }

    public void setDescripcionMarca(String descripcionMarca) {
        this.descripcionMarca = descripcionMarca;
    }

    public String getDescripcionMarca() {
        return descripcionMarca;
    }

    public void setCodigoClase(Integer codigoClase) {
        this.codigoClase = codigoClase;
    }

    public Integer getCodigoClase() {
        return codigoClase;
    }

    public void setDescripcionClase(String descripcionClase) {
        this.descripcionClase = descripcionClase;
    }

    public String getDescripcionClase() {
        return descripcionClase;
    }


    public void setCodigoLinea(Integer codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    public Integer getCodigoLinea() {
        return codigoLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setCodigoModelo(Integer codigoModelo) {
        this.codigoModelo = codigoModelo;
    }

    public Integer getCodigoModelo() {
        return codigoModelo;
    }

    public void setDescripcionModelo(String descripcionModelo) {
        this.descripcionModelo = descripcionModelo;
    }

    public String getDescripcionModelo() {
        return descripcionModelo;
    }

    public void setCodigoCiudad(Integer codigoCiudad) {
        this.codigoCiudad = codigoCiudad;
    }

    public Integer getCodigoCiudad() {
        return codigoCiudad;
    }

    public void setDescripcionCiudad(String descripcionCiudad) {
        this.descripcionCiudad = descripcionCiudad;
    }

    public String getDescripcionCiudad() {
        return descripcionCiudad;
    }

    public void setCodigoModalidadTransporte(Integer codigoModalidadTransporte) {
        this.codigoModalidadTransporte = codigoModalidadTransporte;
    }

    public Integer getCodigoModalidadTransporte() {
        return codigoModalidadTransporte;
    }

    public void setDescripcionModalidadTransporte(String descripcionModalidadTransporte) {
        this.descripcionModalidadTransporte = descripcionModalidadTransporte;
    }

    public String getDescripcionModalidadTransporte() {
        return descripcionModalidadTransporte;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setRobado(Boolean robado) {
        this.robado = robado;
    }

    public Boolean getRobado() {
        return robado;
    }

}
