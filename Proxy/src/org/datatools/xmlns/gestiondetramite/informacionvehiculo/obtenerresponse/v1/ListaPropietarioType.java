
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListaPropietarioType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ListaPropietarioType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Propietario" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}PropietarioType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaPropietarioType", propOrder = { "propietario" })
public class ListaPropietarioType {

    @XmlElement(name = "Propietario")
    protected List<PropietarioType> propietario;

    /**
     * Gets the value of the propietario property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propietario property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropietario().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropietarioType }
     *
     *
     */
    public List<PropietarioType> getPropietario() {
        if (propietario == null) {
            propietario = new ArrayList<PropietarioType>();
        }
        return this.propietario;
    }

}
