
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerrequest.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Placa" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerRequest/V1.0}PlacaType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "placa" })
@XmlRootElement(name = "GestionDeTramite_ObtenerInformacionVehiculoMsgReq")
public class GestionDeTramiteObtenerInformacionVehiculoMsgReq {

    @XmlElement(name = "Placa", required = true)
    protected PlacaType placa;

    /**
     * Gets the value of the placa property.
     *
     * @return
     *     possible object is
     *     {@link PlacaType }
     *
     */
    public PlacaType getPlaca() {
        return placa;
    }

    /**
     * Sets the value of the placa property.
     *
     * @param value
     *     allowed object is
     *     {@link PlacaType }
     *
     */
    public void setPlaca(PlacaType value) {
        this.placa = value;
    }

}
