package org.ws.facade;

import com.librerias.JSFUtil;

import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;

import org.datatools.xmlns.gestiondetramite.listadosformulario.cargar.v1.CargarListadosFormulariosPt;
import org.datatools.xmlns.gestiondetramite.listadosformulario.cargar.v1.GestionDeTramiteCargarListadosFormularioServiceImpl;
import org.datatools.xmlns.gestiondetramite.listadosformulario.cargarrequest.v1.GestionDeTramiteConsultarTiposIdentificacionMsgReq;
import org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1.GestionDeTramiteConsultarTiposIdentificacionMsgResp;
import org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1.TipoIdentificacionType;


public class TipoIdentificacionFacade {
    public TipoIdentificacionFacade() {
        super();
    }
    
    public static String getTipoIdentificacionWs(List<SelectItem> list){
        try{
            GestionDeTramiteCargarListadosFormularioServiceImpl gestionDeTramiteCargarListadosFormularioServiceImpl =
                new GestionDeTramiteCargarListadosFormularioServiceImpl();
            CargarListadosFormulariosPt cargarListadosFormulariosPt =
                gestionDeTramiteCargarListadosFormularioServiceImpl.getGestionDeTramiteCargarListadosFormularioPortImpl();
            
            GestionDeTramiteConsultarTiposIdentificacionMsgResp type = cargarListadosFormulariosPt.consultarTiposIdentificacionOp(new GestionDeTramiteConsultarTiposIdentificacionMsgReq());
            Iterator<TipoIdentificacionType> it = type.getTipoIdentificacion().iterator();
            while(it.hasNext()){
                TipoIdentificacionType paramIdentificacion = it.next();
                list.add(new SelectItem(Integer.parseInt(paramIdentificacion.getIdTipo()), 
                                        paramIdentificacion.getNombreTipo()));
            }
        }catch(Exception e){
            JSFUtil.addErrorMessage("No se ha conectado al servicio correctamente ! : "+e.getMessage());
        }
        
        if(list.size()>0){
            return "Conexion Exitosa";
        }else{
            return "Error en la conexion";
        }
        
    }
    
    
}
