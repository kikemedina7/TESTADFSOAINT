package com.modelo;

import java.io.Serializable;

import java.util.List;

public class Propietario extends Persona implements Serializable{
    @SuppressWarnings("compatibility:1696650109916769121")
    private static final long serialVersionUID = 1L;
    
    private Integer idPropietario;
    private Persona persona;
    private String email;
    private List<Telefono> listaTelefono;
    private List<Direccion> listaDireccion;

    public Propietario() {
        super();
    }


    public Propietario(Integer idPropietario, Persona persona, String email, List<Telefono> listaTelefono,
                       List<Direccion> listaDireccion) {
        super();
        this.idPropietario = idPropietario;
        this.persona = persona;
        this.email = email;
        this.listaTelefono = listaTelefono;
        this.listaDireccion = listaDireccion;
    }

    public void setIdPropietario(Integer idPropietario) {
        this.idPropietario = idPropietario;
    }

    public Integer getIdPropietario() {
        return idPropietario;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setListaTelefono(List<Telefono> listaTelefono) {
        this.listaTelefono = listaTelefono;
    }

    public List<Telefono> getListaTelefono() {
        return listaTelefono;
    }

    public void setListaDireccion(List<Direccion> listaDireccion) {
        this.listaDireccion = listaDireccion;
    }

    public List<Direccion> getListaDireccion() {
        return listaDireccion;
    }

}
