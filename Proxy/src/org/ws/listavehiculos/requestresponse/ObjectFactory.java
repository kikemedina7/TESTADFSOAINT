
package org.ws.listavehiculos.requestresponse;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.ws.listavehiculos.requestresponse package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.ws.listavehiculos.requestresponse
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarModalidadTransporteInformacionVehiculoMsgReq }
     *
     */
    public ConsultarModalidadTransporteInformacionVehiculoMsgReq createConsultarModalidadTransporteInformacionVehiculoMsgReq() {
        return new ConsultarModalidadTransporteInformacionVehiculoMsgReq();
    }

    /**
     * Create an instance of {@link ConsultarClaseInformacionVehiculoMsgReq }
     *
     */
    public ConsultarClaseInformacionVehiculoMsgReq createConsultarClaseInformacionVehiculoMsgReq() {
        return new ConsultarClaseInformacionVehiculoMsgReq();
    }

    /**
     * Create an instance of {@link ConsultarMarcaInformacionVehiculoMsgReq }
     *
     */
    public ConsultarMarcaInformacionVehiculoMsgReq createConsultarMarcaInformacionVehiculoMsgReq() {
        return new ConsultarMarcaInformacionVehiculoMsgReq();
    }

    /**
     * Create an instance of {@link ConsultarLineaInformacionVehiculoMsgReq }
     *
     */
    public ConsultarLineaInformacionVehiculoMsgReq createConsultarLineaInformacionVehiculoMsgReq() {
        return new ConsultarLineaInformacionVehiculoMsgReq();
    }

    /**
     * Create an instance of {@link ConsultarModeloInformacionVehiculoMsgReq }
     *
     */
    public ConsultarModeloInformacionVehiculoMsgReq createConsultarModeloInformacionVehiculoMsgReq() {
        return new ConsultarModeloInformacionVehiculoMsgReq();
    }

    /**
     * Create an instance of {@link ConsultarModalidadTransporteInformacionVehiculoMsgResp }
     *
     */
    public ConsultarModalidadTransporteInformacionVehiculoMsgResp createConsultarModalidadTransporteInformacionVehiculoMsgResp() {
        return new ConsultarModalidadTransporteInformacionVehiculoMsgResp();
    }

    /**
     * Create an instance of {@link GenericItemType }
     *
     */
    public GenericItemType createGenericItemType() {
        return new GenericItemType();
    }

    /**
     * Create an instance of {@link ConsultarClaseInformacionVehiculoMsgResp }
     *
     */
    public ConsultarClaseInformacionVehiculoMsgResp createConsultarClaseInformacionVehiculoMsgResp() {
        return new ConsultarClaseInformacionVehiculoMsgResp();
    }

    /**
     * Create an instance of {@link ConsultarMarcaInformacionVehiculoMsgResp }
     *
     */
    public ConsultarMarcaInformacionVehiculoMsgResp createConsultarMarcaInformacionVehiculoMsgResp() {
        return new ConsultarMarcaInformacionVehiculoMsgResp();
    }

    /**
     * Create an instance of {@link ConsultarLineaInformacionVehiculoMsgResp }
     *
     */
    public ConsultarLineaInformacionVehiculoMsgResp createConsultarLineaInformacionVehiculoMsgResp() {
        return new ConsultarLineaInformacionVehiculoMsgResp();
    }

    /**
     * Create an instance of {@link ConsultarModeloInformacionVehiculoMsgResp }
     *
     */
    public ConsultarModeloInformacionVehiculoMsgResp createConsultarModeloInformacionVehiculoMsgResp() {
        return new ConsultarModeloInformacionVehiculoMsgResp();
    }

}
