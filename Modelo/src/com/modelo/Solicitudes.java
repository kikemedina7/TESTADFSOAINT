package com.modelo;

import java.io.Serializable;

import java.util.Date;

public class Solicitudes implements Serializable{
    @SuppressWarnings("compatibility:1185155983973459125")
    private static final long serialVersionUID = 1L;

    private Integer numeroSolicitud;
    private Integer tipoSolicitud;
    private String descripcionTipoSolicitud;
    private Date fechaRegistro;
    private Boolean activo;
    
    public Solicitudes() {
        super();
    }


    public Solicitudes(Integer numeroSolicitud, Integer tipoSolicitud, String descripcionTipoSolicitud,
                       Date fechaRegistro, Boolean activo) {
        this.numeroSolicitud = numeroSolicitud;
        this.tipoSolicitud = tipoSolicitud;
        this.descripcionTipoSolicitud = descripcionTipoSolicitud;
        this.fechaRegistro = fechaRegistro;
        this.activo = activo;
    }


    public void setNumeroSolicitud(Integer numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public Integer getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setTipoSolicitud(Integer tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public Integer getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setDescripcionTipoSolicitud(String descripcionTipoSolicitud) {
        this.descripcionTipoSolicitud = descripcionTipoSolicitud;
    }

    public String getDescripcionTipoSolicitud() {
        return descripcionTipoSolicitud;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getActivo() {
        return activo;
    }

}
