
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarTipoPersonaMsgResp }
     *
     */
    public GestionDeTramiteConsultarTipoPersonaMsgResp createGestionDeTramiteConsultarTipoPersonaMsgResp() {
        return new GestionDeTramiteConsultarTipoPersonaMsgResp();
    }

    /**
     * Create an instance of {@link TipoPersonaType }
     *
     */
    public TipoPersonaType createTipoPersonaType() {
        return new TipoPersonaType();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarTiposIdentificacionMsgResp }
     *
     */
    public GestionDeTramiteConsultarTiposIdentificacionMsgResp createGestionDeTramiteConsultarTiposIdentificacionMsgResp() {
        return new GestionDeTramiteConsultarTiposIdentificacionMsgResp();
    }

    /**
     * Create an instance of {@link TipoIdentificacionType }
     *
     */
    public TipoIdentificacionType createTipoIdentificacionType() {
        return new TipoIdentificacionType();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarListadoTramitesMsgResp }
     *
     */
    public GestionDeTramiteConsultarListadoTramitesMsgResp createGestionDeTramiteConsultarListadoTramitesMsgResp() {
        return new GestionDeTramiteConsultarListadoTramitesMsgResp();
    }

    /**
     * Create an instance of {@link TramiteType }
     *
     */
    public TramiteType createTramiteType() {
        return new TramiteType();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarMediosComunicacionMsgResp }
     *
     */
    public GestionDeTramiteConsultarMediosComunicacionMsgResp createGestionDeTramiteConsultarMediosComunicacionMsgResp() {
        return new GestionDeTramiteConsultarMediosComunicacionMsgResp();
    }

    /**
     * Create an instance of {@link MedioComunicacionType }
     *
     */
    public MedioComunicacionType createMedioComunicacionType() {
        return new MedioComunicacionType();
    }

}
