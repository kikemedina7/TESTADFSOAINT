package controller.fragments;

import com.librerias.JSFUtil;
import com.librerias.LoggerUtil;

import com.modelo.Direccion;
import com.modelo.Persona;
import com.modelo.Propietario;
import com.modelo.Telefono;
import com.modelo.Vehiculo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ws.facade.ListasVehiculosFacade;
import org.ws.facade.TipoIdentificacionFacade;
import org.ws.facade.VehiculosFacade;

public class VehiculosBean {
    
    private List<Propietario> listaPropietario;
    private String placaSeleccionada;
    private Vehiculo vehiculo;
    private List<SelectItem> listaClase;
    private Integer claseSelecionada;
    private List<SelectItem> listaLinea;
    private Integer lineaSelecionada;
    private List<SelectItem> listaMarca;
    private Integer marcaSelecionada;
    private List<SelectItem> listaModalidad;
    private Integer modalidadSelecionada;
    private List<SelectItem> listaModelo;
    private Integer modeloSelecionado;
    private List<SelectItem> listaTipoIdentificacionSelectItem;
    private List<SelectItem> listaAlertasLegales;
    private Integer alertaSeleccionada;
    private List<Direccion> listaDireccion;
    private List<Telefono> listaTelefono;

    public VehiculosBean() {
        super();
        this.listaPropietario = new ArrayList<Propietario>();
        this.listaDireccion = new ArrayList<Direccion>();
        this.listaTelefono = new ArrayList<Telefono>();
        this.vehiculo = new Vehiculo();
        this.listaClase = new ArrayList<SelectItem>();
        this.listaLinea = new ArrayList<SelectItem>();
        this.listaMarca = new ArrayList<SelectItem>();
        this.listaModalidad = new ArrayList<SelectItem>();
        this.listaModelo = new ArrayList<SelectItem>();
        this.listaTipoIdentificacionSelectItem = new ArrayList<SelectItem>();
        this.listaAlertasLegales = new ArrayList<SelectItem>();
        
        cargaInicial();
    }
    
    private void cargaInicial() {
                
        //Busca informacion de las listas que se deben cargar        
        String msg = ListasVehiculosFacade.getListasParaVehiculo(this.listaClase, this.listaLinea, this.listaMarca, this.listaModalidad, this.listaModelo);
        JSFUtil.addFacesInformationMessage(msg);
        LoggerUtil.sendLogMessage("Servicio ListasVehiculosFacade", msg); 
        
        //Carga las lista manuales que no estan en el WS
        this.listaAlertasLegales.add(new SelectItem(1,"Yes"));
        this.listaAlertasLegales.add(new SelectItem(2,"No"));
        
    }
    
    public void buscarInformacionPorPlaca(ActionEvent event){
        
        //Limpia las listas 
        this.listaPropietario.clear();
        
        //Busca informacion de la placa seleccionada 
        String msg = VehiculosFacade.getPropietariosList(this.listaPropietario,this.vehiculo,this.placaSeleccionada);
        JSFUtil.addFacesInformationMessage(msg);
        LoggerUtil.sendLogMessage("Servicio VehiculosFacade", msg);
        
        //Busca la lista de los tipo de identificacion 
        msg = TipoIdentificacionFacade.getTipoIdentificacionWs(this.listaTipoIdentificacionSelectItem);
        LoggerUtil.sendLogMessage("Servicio TipoIdentificacionFacade",msg);
        
        //Actualizando los valores de las listas
        this.claseSelecionada = this.vehiculo.getCodigoClase();
        this.lineaSelecionada = this.vehiculo.getCodigoLinea();
        this.marcaSelecionada = this.vehiculo.getCodigoMarca();
        this.modalidadSelecionada = this.vehiculo.getCodigoModalidadTransporte();
        this.modeloSelecionado = this.vehiculo.getCodigoModelo();
        this.alertaSeleccionada = 1;
        
        //Actualizando y llenando las listas de direccion y telefono
        Iterator<Propietario> itProp = this.listaPropietario.iterator();
        while(itProp.hasNext()){
            Propietario prop = itProp.next();
            Iterator<Telefono> itTlf = prop.getListaTelefono().iterator();
            while(itTlf.hasNext()){
                Telefono tlf = itTlf.next();
                this.listaTelefono.add(tlf);
            }
            Iterator<Direccion> itDir = prop.getListaDireccion().iterator();
            while(itDir.hasNext()){
                Direccion dir = itDir.next();
                this.listaDireccion.add(dir);
            }            
        }
        
        
    }


    public void setListaPropietario(List<Propietario> listaPropietario) {
        this.listaPropietario = listaPropietario;
    }

    public List<Propietario> getListaPropietario() {
        return listaPropietario;
    }

    public void setPlacaSeleccionada(String placaSeleccionada) {
        this.placaSeleccionada = placaSeleccionada;
    }

    public String getPlacaSeleccionada() {
        return placaSeleccionada;
    }


    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }


    public void setListaClase(List<SelectItem> listaClase) {
        this.listaClase = listaClase;
    }

    public List<SelectItem> getListaClase() {
        return listaClase;
    }

    public void setClaseSelecionada(Integer claseSelecionada) {
        this.claseSelecionada = claseSelecionada;
    }

    public Integer getClaseSelecionada() {
        return claseSelecionada;
    }

    public void setListaLinea(List<SelectItem> listaLinea) {
        this.listaLinea = listaLinea;
    }

    public List<SelectItem> getListaLinea() {
        return listaLinea;
    }

    public void setLineaSelecionada(Integer lineaSelecionada) {
        this.lineaSelecionada = lineaSelecionada;
    }

    public Integer getLineaSelecionada() {
        return lineaSelecionada;
    }

    public void setListaMarca(List<SelectItem> listaMarca) {
        this.listaMarca = listaMarca;
    }

    public List<SelectItem> getListaMarca() {
        return listaMarca;
    }

    public void setMarcaSelecionada(Integer marcaSelecionada) {
        this.marcaSelecionada = marcaSelecionada;
    }

    public Integer getMarcaSelecionada() {
        return marcaSelecionada;
    }

    public void setListaModalidad(List<SelectItem> listaModalidad) {
        this.listaModalidad = listaModalidad;
    }

    public List<SelectItem> getListaModalidad() {
        return listaModalidad;
    }

    public void setModalidadSelecionada(Integer modalidadSelecionada) {
        this.modalidadSelecionada = modalidadSelecionada;
    }

    public Integer getModalidadSelecionada() {
        return modalidadSelecionada;
    }

    public void setListaModelo(List<SelectItem> listaModelo) {
        this.listaModelo = listaModelo;
    }

    public List<SelectItem> getListaModelo() {
        return listaModelo;
    }

    public void setModeloSelecionado(Integer modeloSelecionado) {
        this.modeloSelecionado = modeloSelecionado;
    }

    public Integer getModeloSelecionado() {
        return modeloSelecionado;
    }


    public void setListaTipoIdentificacionSelectItem(List<SelectItem> listaTipoIdentificacionSelectItem) {
        this.listaTipoIdentificacionSelectItem = listaTipoIdentificacionSelectItem;
    }

    public List<SelectItem> getListaTipoIdentificacionSelectItem() {
        return listaTipoIdentificacionSelectItem;
    }


    public void setListaAlertasLegales(List<SelectItem> listaAlertasLegales) {
        this.listaAlertasLegales = listaAlertasLegales;
    }

    public List<SelectItem> getListaAlertasLegales() {
        return listaAlertasLegales;
    }

    public void setAlertaSeleccionada(Integer alertaSeleccionada) {
        this.alertaSeleccionada = alertaSeleccionada;
    }

    public Integer getAlertaSeleccionada() {
        return alertaSeleccionada;
    }


    public void setListaDireccion(List<Direccion> listaDireccion) {
        this.listaDireccion = listaDireccion;
    }

    public List<Direccion> getListaDireccion() {
        return listaDireccion;
    }

    public void setListaTelefono(List<Telefono> listaTelefono) {
        this.listaTelefono = listaTelefono;
    }

    public List<Telefono> getListaTelefono() {
        return listaTelefono;
    }


    public void eliminarPropietario(ActionEvent actionEvent) {
        // Add event code here...
        int id = (Integer) actionEvent.getComponent().getAttributes().get("eliminarPropietario");
        System.out.println("**************" + id);
        Iterator it = listaPropietario.iterator();

        for (int i = 0; i < listaPropietario.size(); i++) {
            Propietario em = (Propietario) it.next();
            if (em.getIdPropietario() == id) {
                listaPropietario.remove(i);
            }
        }
    }
    
    public void agregarPropietario(ActionEvent event){
        LoggerUtil.sendLogMessage("agregarPropietario", "Agregando propietario vacio");
        Propietario prop = this.listaPropietario.get(this.listaPropietario.size()-1);
        Integer newIdPropietario = prop.getIdPropietario()+1;
        this.listaPropietario.add(new Propietario(newIdPropietario,
                                                  new Persona(), 
                                                  "", 
                                                  new ArrayList<Telefono>(), 
                                                  new ArrayList<Direccion>()));
    }
}
