package controller.fragments;

import com.librerias.LoggerUtil;

import com.modelo.*;

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.*;

public class SolicitudesBean implements Serializable{
    @SuppressWarnings("compatibility:5511856123604889685")
    private static final long serialVersionUID = 1L;

    private List<Solicitudes> listaSolicitudes;
    
    public SolicitudesBean() {
        super();
        cargaInicial();
    }


    public void setListaSolicitudes(List<Solicitudes> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public List<Solicitudes> getListaSolicitudes() {
        return listaSolicitudes;
    }
    

    private void cargaInicial() {
        
        this.listaSolicitudes = new ArrayList<Solicitudes>();
        
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        try{
            this.listaSolicitudes.add(new Solicitudes(001, 1, "Solicitud del primer cliente", formateador.parse("12/01/2018"), Boolean.TRUE));
            this.listaSolicitudes.add(new Solicitudes(002, 1, "Solicitud del segundo cliente", formateador.parse("16/01/2018"), Boolean.FALSE));
        }catch(ParseException e){
            LoggerUtil.sendLogMessage("Fecha",e.getMessage());
        } 
    }
}
