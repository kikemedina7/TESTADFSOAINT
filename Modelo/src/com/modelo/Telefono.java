package com.modelo;

import java.io.Serializable;

public class Telefono implements Serializable{
    @SuppressWarnings("compatibility:-9066313038841060986")
    private static final long serialVersionUID = 1L;

    private Integer codTipoTelefono;
    private String descTipoTelefono;
    private Integer indicativo;
    private Integer codTelefono;
    private Integer numeroTelefono;
    
    public Telefono() {
        super();
    }


    public Telefono(Integer codTipoTelefono, String descTipoTelefono, Integer indicativo, Integer codTelefono,
                    Integer numeroTelefono) {
        this.codTipoTelefono = codTipoTelefono;
        this.descTipoTelefono = descTipoTelefono;
        this.indicativo = indicativo;
        this.codTelefono = codTelefono;
        this.numeroTelefono = numeroTelefono;
    }

    public void setCodTipoTelefono(Integer codTipoTelefono) {
        this.codTipoTelefono = codTipoTelefono;
    }

    public Integer getCodTipoTelefono() {
        return codTipoTelefono;
    }

    public void setDescTipoTelefono(String descTipoTelefono) {
        this.descTipoTelefono = descTipoTelefono;
    }

    public String getDescTipoTelefono() {
        return descTipoTelefono;
    }

    public void setIndicativo(Integer indicativo) {
        this.indicativo = indicativo;
    }

    public Integer getIndicativo() {
        return indicativo;
    }

    public void setCodTelefono(Integer codTelefono) {
        this.codTelefono = codTelefono;
    }

    public Integer getCodTelefono() {
        return codTelefono;
    }

    public void setNumeroTelefono(Integer numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public Integer getNumeroTelefono() {
        return numeroTelefono;
    }

}
