package org.ws.facade;

import com.modelo.Direccion;
import com.modelo.Propietario;
import com.modelo.Telefono;
import com.modelo.Vehiculo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.datatools.xmlns.gestiondetramite.informacionvehiculo.cargar.BpelConsultarvehiculoV1ClientEp;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.cargar.ObtenerInformacionVehiculoPt;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerrequest.v1.GestionDeTramiteObtenerInformacionVehiculoMsgReq;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerrequest.v1.PlacaType;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1.DireccionType;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1.GestionDeTramiteObtenerInformacionVehiculoMsgResp;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1.PropietarioType;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1.TelefonoType;
import org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1.VehiculoType;

public class VehiculosFacade {
    
    public VehiculosFacade() {
        super();
    }
    
    public static String getPropietariosList(List<Propietario> listaPropietario, Vehiculo vehiculo,String numeroPlaca){
        
        BpelConsultarvehiculoV1ClientEp bpelConsultarvehiculoV1ClientEp = new BpelConsultarvehiculoV1ClientEp();
        ObtenerInformacionVehiculoPt obtenerInformacionVehiculoPt =
            bpelConsultarvehiculoV1ClientEp.getObtenerInformacionVehiculoPtPt();
        
        GestionDeTramiteObtenerInformacionVehiculoMsgReq req = new GestionDeTramiteObtenerInformacionVehiculoMsgReq();
        PlacaType placa = new PlacaType();
        placa.setNroPlaca(numeroPlaca);
        req.setPlaca(placa);
        GestionDeTramiteObtenerInformacionVehiculoMsgResp type = obtenerInformacionVehiculoPt.obtenerInformacionVehiculoOp(req);
       
        //Obteniendo Vehiculos
        VehiculoType vh = type.getVehiculo();
        setVehiculoFacade(vehiculo,vh);

        //Propietarios
        Iterator<PropietarioType> it = type.getListaPropietario().getPropietario().iterator();
        
        while(it.hasNext()){
            
            listaPropietario.add(setListaPropietarios(it.next()));
            
        }
        
        System.out.println(type.getVehiculo().getClaseVehiculo().getDescripcion());
        System.out.println(type.getVehiculo().getLinea().getDescripcion());
        
        if(!listaPropietario.isEmpty() && vehiculo.getPlaca() != ""){
            return "Busqueda Exitosa";
        }else{
            if(listaPropietario.isEmpty()){
                return "No tiene propietarios";
            }
            if(vehiculo.getPlaca().equals("")){
                return "No tiene vehiculo asignado la placa";
            }
        }
        
        return "No tiene informacion esta placa";
       
    }


    private static void setVehiculoFacade(Vehiculo vehiculo, VehiculoType vehiculoWs) {
        
        vehiculo.setIdVehiculo(vehiculoWs.getIdVehiculo());
        vehiculo.setCodigoClase(vehiculoWs.getClaseVehiculo().getCodigo());
        vehiculo.setCodigoCiudad(vehiculoWs.getCiudad().getCodigo());
        vehiculo.setCodigoLinea(vehiculoWs.getLinea().getCodigo());
        vehiculo.setCodigoMarca(vehiculoWs.getMarca().getCodigo());
        vehiculo.setCodigoModalidadTransporte(vehiculoWs.getModalidadTransporte().getCodigo());
        vehiculo.setCodigoModelo(vehiculoWs.getModelo().getCodigo());
        
        vehiculo.setAlertasLegales(vehiculoWs.getAlertasLegales());
        vehiculo.setDescripcionClase(vehiculoWs.getClaseVehiculo().getDescripcion());
        vehiculo.setDescripcionCiudad(vehiculoWs.getCiudad().getDescripcion());
        vehiculo.setDescripcionLinea(vehiculoWs.getLinea().getDescripcion());
        vehiculo.setDescripcionMarca(vehiculoWs.getMarca().getDescripcion());
        vehiculo.setDescripcionModalidadTransporte(vehiculoWs.getModalidadTransporte().getDescripcion());
        vehiculo.setDescripcionModelo(vehiculoWs.getModelo().getDescripcion());
                
        
    }


    private static Propietario setListaPropietarios(PropietarioType propietarioType) {
        
        Propietario propietario = new Propietario();
        propietario.setIdPropietario(propietarioType.getIdpersona());
        propietario.setCodigoTipoIdentificacion(propietarioType.getCodTipoIdentificacion());
        propietario.setDescripcionTipoIdentificacion(propietarioType.getDescTipoIdentificacion());
        propietario.setEmail(propietarioType.getEmail());
        propietario.setNumeroidentificacion(Integer.parseInt(propietarioType.getNroIdentificacion()));
        
        //Tratando el nombre que viene del servicio
        String[] arrayNombre = propietarioType.getRazonSocial().split(" ");
        
        propietario.setPrimernombre(arrayNombre[0]);
        propietario.setSegundonombre(arrayNombre[1]);
        propietario.setPrimerapellido(arrayNombre[2]);
        propietario.setSegundoapellido(arrayNombre[3]);
        
       
        //Añadiendo la lista de la direccion al propietario
        
        Iterator<DireccionType> itDireccion = propietarioType.getDireccion().iterator();
        List<Direccion> listaDireccion = new ArrayList<Direccion>();
        
        while(itDireccion.hasNext()){
            listaDireccion.add(setDirecionPropietario(itDireccion.next()));
        }
        
        propietario.setListaDireccion(listaDireccion);
        
        //Añadiendo la lista de los telefonos al propietario
        
        Iterator<TelefonoType> itTelefono = propietarioType.getTelefono().iterator();
        List<Telefono> listaTelefono = new ArrayList<Telefono>();
        
        while(itTelefono.hasNext()){
            listaTelefono.add(setTelefonoPropietario(itTelefono.next()));
        }
        
        propietario.setListaTelefono(listaTelefono);
        
        
        return propietario;
    }

    private static Direccion setDirecionPropietario(DireccionType direccionType) {
        Direccion dir = new Direccion();
        
            dir.setCodCiudad(direccionType.getCodCiudad());
            dir.setCodDepartamento(direccionType.getCodDepartamento());
            dir.setCodPais(direccionType.getCodPais());
            dir.setDescCiudad(direccionType.getDescCiudad());
            dir.setDescDepartamento(direccionType.getDescDepartamento());
            dir.setDescPais(direccionType.getDescPais());
            dir.setDireccion(direccionType.getDireccion());
            
        return dir;
    }

    private static Telefono setTelefonoPropietario(TelefonoType telefonoType) {
        Telefono tlf = new Telefono();
        
        tlf.setCodTelefono(telefonoType.getCodTelefono());
        tlf.setCodTipoTelefono(telefonoType.getCodTipoTelefono());
        tlf.setDescTipoTelefono(telefonoType.getDescTipoTelefono());
        tlf.setIndicativo(telefonoType.getIndicativo());
        tlf.setNumeroTelefono(telefonoType.getTelefono());
   
        return tlf;
        
    }
}
