
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlacaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PlacaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nroPlaca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlacaType", propOrder = { "nroPlaca" })
public class PlacaType {

    protected String nroPlaca;

    /**
     * Gets the value of the nroPlaca property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNroPlaca() {
        return nroPlaca;
    }

    /**
     * Sets the value of the nroPlaca property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNroPlaca(String value) {
        this.nroPlaca = value;
    }

}
