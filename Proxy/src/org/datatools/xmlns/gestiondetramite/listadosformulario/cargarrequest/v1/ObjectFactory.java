
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarrequest.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.datatools.xmlns.gestiondetramite.listadosformulario.cargarrequest.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datatools.xmlns.gestiondetramite.listadosformulario.cargarrequest.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarTipoPersonaMsgReq }
     *
     */
    public GestionDeTramiteConsultarTipoPersonaMsgReq createGestionDeTramiteConsultarTipoPersonaMsgReq() {
        return new GestionDeTramiteConsultarTipoPersonaMsgReq();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarTiposIdentificacionMsgReq }
     *
     */
    public GestionDeTramiteConsultarTiposIdentificacionMsgReq createGestionDeTramiteConsultarTiposIdentificacionMsgReq() {
        return new GestionDeTramiteConsultarTiposIdentificacionMsgReq();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarListadoTramitesMsgReq }
     *
     */
    public GestionDeTramiteConsultarListadoTramitesMsgReq createGestionDeTramiteConsultarListadoTramitesMsgReq() {
        return new GestionDeTramiteConsultarListadoTramitesMsgReq();
    }

    /**
     * Create an instance of {@link GestionDeTramiteConsultarMediosComunicacionMsgReq }
     *
     */
    public GestionDeTramiteConsultarMediosComunicacionMsgReq createGestionDeTramiteConsultarMediosComunicacionMsgReq() {
        return new GestionDeTramiteConsultarMediosComunicacionMsgReq();
    }

}
