package com.modelo;

import java.io.Serializable;

import java.util.Date;


public class Persona implements Serializable{
    @SuppressWarnings("compatibility:124999344105456400")
    private static final long serialVersionUID = 1L;

    private String primernombre;
    private String segundonombre;
    private Integer numeroidentificacion;
    private Integer codigoTipoIdentificacion;
    private String descripcionTipoIdentificacion;
    private String primerapellido;
    private String segundoapellido;
    private Integer edad;
    private Date fecharegistro;
    private Double ingresos;
    private Boolean activo;
    
    public Persona() {
        super();
    }


    public Persona(String primernombre, String segundonombre, Integer numeroidentificacion,
                   Integer codigoTipoIdentificacion, String descripcionTipoIdentificacion, String primerapellido,
                   String segundoapellido, Integer edad, Date fecharegistro, Double ingresos, Boolean activo) {
        this.primernombre = primernombre;
        this.segundonombre = segundonombre;
        this.numeroidentificacion = numeroidentificacion;
        this.codigoTipoIdentificacion = codigoTipoIdentificacion;
        this.descripcionTipoIdentificacion = descripcionTipoIdentificacion;
        this.primerapellido = primerapellido;
        this.segundoapellido = segundoapellido;
        this.edad = edad;
        this.fecharegistro = fecharegistro;
        this.ingresos = ingresos;
        this.activo = activo;
    }


    public void setPrimernombre(String primernombre) {
        this.primernombre = primernombre;
    }

    public String getPrimernombre() {
        return primernombre;
    }

    public void setSegundonombre(String segundonombre) {
        this.segundonombre = segundonombre;
    }

    public String getSegundonombre() {
        return segundonombre;
    }

    public void setNumeroidentificacion(Integer numeroidentificacion) {
        this.numeroidentificacion = numeroidentificacion;
    }

    public Integer getNumeroidentificacion() {
        return numeroidentificacion;
    }

    public void setCodigoTipoIdentificacion(Integer codigoTipoIdentificacion) {
        this.codigoTipoIdentificacion = codigoTipoIdentificacion;
    }

    public Integer getCodigoTipoIdentificacion() {
        return codigoTipoIdentificacion;
    }

    public void setDescripcionTipoIdentificacion(String descripcionTipoIdentificacion) {
        this.descripcionTipoIdentificacion = descripcionTipoIdentificacion;
    }

    public String getDescripcionTipoIdentificacion() {
        return descripcionTipoIdentificacion;
    }

    public void setPrimerapellido(String primerapellido) {
        this.primerapellido = primerapellido;
    }

    public String getPrimerapellido() {
        return primerapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setIngresos(Double ingresos) {
        this.ingresos = ingresos;
    }

    public Double getIngresos() {
        return ingresos;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getActivo() {
        return activo;
    }

}
