package com.modelo;

import java.io.Serializable;

public class Direccion implements Serializable{
    @SuppressWarnings("compatibility:3440898416525221250")
    private static final long serialVersionUID = 1L;

    private Integer codPais;
    private String descPais;
    private Integer codDepartamento;
    private String descDepartamento;
    private Integer codCiudad;
    private String descCiudad;
    private String direccion;
    
    public Direccion() {
        super();
    }


    public Direccion(Integer codPais, String descPais, Integer codDepartamento, String descDepartamento,
                     Integer codCiudad, String descCiudad, String direccion) {
        this.codPais = codPais;
        this.descPais = descPais;
        this.codDepartamento = codDepartamento;
        this.descDepartamento = descDepartamento;
        this.codCiudad = codCiudad;
        this.descCiudad = descCiudad;
        this.direccion = direccion;
    }

    public void setCodPais(Integer codPais) {
        this.codPais = codPais;
    }

    public Integer getCodPais() {
        return codPais;
    }

    public void setDescPais(String descPais) {
        this.descPais = descPais;
    }

    public String getDescPais() {
        return descPais;
    }

    public void setCodDepartamento(Integer codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public Integer getCodDepartamento() {
        return codDepartamento;
    }

    public void setDescDepartamento(String descDepartamento) {
        this.descDepartamento = descDepartamento;
    }

    public String getDescDepartamento() {
        return descDepartamento;
    }

    public void setCodCiudad(Integer codCiudad) {
        this.codCiudad = codCiudad;
    }

    public Integer getCodCiudad() {
        return codCiudad;
    }

    public void setDescCiudad(String descCiudad) {
        this.descCiudad = descCiudad;
    }

    public String getDescCiudad() {
        return descCiudad;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }
}
