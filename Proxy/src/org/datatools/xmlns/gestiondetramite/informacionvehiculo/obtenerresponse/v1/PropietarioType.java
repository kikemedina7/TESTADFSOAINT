
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PropietarioType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PropietarioType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idpersona" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="descTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Telefono" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}TelefonoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Direccion" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}DireccionType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PropietarioType", propOrder = {
         "idpersona", "razonSocial", "codTipoIdentificacion", "descTipoIdentificacion", "nroIdentificacion", "email",
         "telefono", "direccion"
    })
public class PropietarioType {

    protected Integer idpersona;
    protected String razonSocial;
    protected Integer codTipoIdentificacion;
    protected String descTipoIdentificacion;
    protected String nroIdentificacion;
    protected String email;
    @XmlElement(name = "Telefono")
    protected List<TelefonoType> telefono;
    @XmlElement(name = "Direccion", required = true)
    protected List<DireccionType> direccion;

    /**
     * Gets the value of the idpersona property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getIdpersona() {
        return idpersona;
    }

    /**
     * Sets the value of the idpersona property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setIdpersona(Integer value) {
        this.idpersona = value;
    }

    /**
     * Gets the value of the razonSocial property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Sets the value of the razonSocial property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Gets the value of the codTipoIdentificacion property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    /**
     * Sets the value of the codTipoIdentificacion property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setCodTipoIdentificacion(Integer value) {
        this.codTipoIdentificacion = value;
    }

    /**
     * Gets the value of the descTipoIdentificacion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescTipoIdentificacion() {
        return descTipoIdentificacion;
    }

    /**
     * Sets the value of the descTipoIdentificacion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescTipoIdentificacion(String value) {
        this.descTipoIdentificacion = value;
    }

    /**
     * Gets the value of the nroIdentificacion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNroIdentificacion() {
        return nroIdentificacion;
    }

    /**
     * Sets the value of the nroIdentificacion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNroIdentificacion(String value) {
        this.nroIdentificacion = value;
    }

    /**
     * Gets the value of the email property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the telefono property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the telefono property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTelefono().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TelefonoType }
     *
     *
     */
    public List<TelefonoType> getTelefono() {
        if (telefono == null) {
            telefono = new ArrayList<TelefonoType>();
        }
        return this.telefono;
    }

    /**
     * Gets the value of the direccion property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the direccion property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDireccion().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DireccionType }
     *
     *
     */
    public List<DireccionType> getDireccion() {
        if (direccion == null) {
            direccion = new ArrayList<DireccionType>();
        }
        return this.direccion;
    }

}
