
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TelefonoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TelefonoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codTipoTelefono" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="descTipoTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="indicativo" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="codTelefono" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelefonoType", propOrder = {
         "codTipoTelefono", "descTipoTelefono", "indicativo", "codTelefono", "telefono" })
public class TelefonoType {

    protected int codTipoTelefono;
    protected String descTipoTelefono;
    protected int indicativo;
    protected int codTelefono;
    @XmlElement(name = "Telefono")
    protected int telefono;

    /**
     * Gets the value of the codTipoTelefono property.
     *
     */
    public int getCodTipoTelefono() {
        return codTipoTelefono;
    }

    /**
     * Sets the value of the codTipoTelefono property.
     *
     */
    public void setCodTipoTelefono(int value) {
        this.codTipoTelefono = value;
    }

    /**
     * Gets the value of the descTipoTelefono property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescTipoTelefono() {
        return descTipoTelefono;
    }

    /**
     * Sets the value of the descTipoTelefono property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescTipoTelefono(String value) {
        this.descTipoTelefono = value;
    }

    /**
     * Gets the value of the indicativo property.
     *
     */
    public int getIndicativo() {
        return indicativo;
    }

    /**
     * Sets the value of the indicativo property.
     *
     */
    public void setIndicativo(int value) {
        this.indicativo = value;
    }

    /**
     * Gets the value of the codTelefono property.
     *
     */
    public int getCodTelefono() {
        return codTelefono;
    }

    /**
     * Sets the value of the codTelefono property.
     *
     */
    public void setCodTelefono(int value) {
        this.codTelefono = value;
    }

    /**
     * Gets the value of the telefono property.
     *
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     *
     */
    public void setTelefono(int value) {
        this.telefono = value;
    }

}
