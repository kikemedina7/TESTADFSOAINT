
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GestionDeTramiteObtenerInformacionVehiculoMsgResp }
     *
     */
    public GestionDeTramiteObtenerInformacionVehiculoMsgResp createGestionDeTramiteObtenerInformacionVehiculoMsgResp() {
        return new GestionDeTramiteObtenerInformacionVehiculoMsgResp();
    }

    /**
     * Create an instance of {@link VehiculoType }
     *
     */
    public VehiculoType createVehiculoType() {
        return new VehiculoType();
    }

    /**
     * Create an instance of {@link ListaPropietarioType }
     *
     */
    public ListaPropietarioType createListaPropietarioType() {
        return new ListaPropietarioType();
    }

    /**
     * Create an instance of {@link GenericItemType }
     *
     */
    public GenericItemType createGenericItemType() {
        return new GenericItemType();
    }

    /**
     * Create an instance of {@link PlacaType }
     *
     */
    public PlacaType createPlacaType() {
        return new PlacaType();
    }

    /**
     * Create an instance of {@link PropietarioType }
     *
     */
    public PropietarioType createPropietarioType() {
        return new PropietarioType();
    }

    /**
     * Create an instance of {@link TelefonoType }
     *
     */
    public TelefonoType createTelefonoType() {
        return new TelefonoType();
    }

    /**
     * Create an instance of {@link DireccionType }
     *
     */
    public DireccionType createDireccionType() {
        return new DireccionType();
    }

}
