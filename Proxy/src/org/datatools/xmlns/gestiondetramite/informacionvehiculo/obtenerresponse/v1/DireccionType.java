
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DireccionType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DireccionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codPais" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DescPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodDepartamento" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DescDepartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codCiudad" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DescCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Direccion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DireccionType", propOrder = {
         "codPais", "descPais", "codDepartamento", "descDepartamento", "codCiudad", "descCiudad", "direccion"
    })
public class DireccionType {

    protected int codPais;
    @XmlElement(name = "DescPais")
    protected String descPais;
    @XmlElement(name = "CodDepartamento")
    protected int codDepartamento;
    @XmlElement(name = "DescDepartamento")
    protected String descDepartamento;
    protected int codCiudad;
    @XmlElement(name = "DescCiudad")
    protected String descCiudad;
    @XmlElement(name = "Direccion", required = true)
    protected String direccion;

    /**
     * Gets the value of the codPais property.
     *
     */
    public int getCodPais() {
        return codPais;
    }

    /**
     * Sets the value of the codPais property.
     *
     */
    public void setCodPais(int value) {
        this.codPais = value;
    }

    /**
     * Gets the value of the descPais property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescPais() {
        return descPais;
    }

    /**
     * Sets the value of the descPais property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescPais(String value) {
        this.descPais = value;
    }

    /**
     * Gets the value of the codDepartamento property.
     *
     */
    public int getCodDepartamento() {
        return codDepartamento;
    }

    /**
     * Sets the value of the codDepartamento property.
     *
     */
    public void setCodDepartamento(int value) {
        this.codDepartamento = value;
    }

    /**
     * Gets the value of the descDepartamento property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescDepartamento() {
        return descDepartamento;
    }

    /**
     * Sets the value of the descDepartamento property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescDepartamento(String value) {
        this.descDepartamento = value;
    }

    /**
     * Gets the value of the codCiudad property.
     *
     */
    public int getCodCiudad() {
        return codCiudad;
    }

    /**
     * Sets the value of the codCiudad property.
     *
     */
    public void setCodCiudad(int value) {
        this.codCiudad = value;
    }

    /**
     * Gets the value of the descCiudad property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescCiudad() {
        return descCiudad;
    }

    /**
     * Sets the value of the descCiudad property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescCiudad(String value) {
        this.descCiudad = value;
    }

    /**
     * Gets the value of the direccion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets the value of the direccion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

}
