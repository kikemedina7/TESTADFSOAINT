package controller.fragments;

import com.librerias.JSFUtil;
import com.librerias.LoggerUtil;

import com.modelo.Persona;

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ws.facade.TipoIdentificacionFacade;

public class PersonaBean implements Serializable{
    @SuppressWarnings("compatibility:-5816408109877604068")
    private static final long serialVersionUID = 1L;

    private List<Persona> listaPersona;
    private List<SelectItem> listaTipoIdentificacionSelectItem;
    private String identificacionSeleccionada;
    private String numeroIdentificacionSeleccionada;
    
//    public static final Log LOGGER = LogFactory.getLog(PersonaBean.class);
     
    
    public PersonaBean() {
        super();
        cargaInicial();       
    }


    public void setListaPersona(List<Persona> listaPersona) {
        this.listaPersona = listaPersona;
    }

    public List<Persona> getListaPersona() {
        return listaPersona;
    }


    public void setListaTipoIdentificacionSelectItem(List<SelectItem> listaTipoIdentificacionSelectItem) {
        this.listaTipoIdentificacionSelectItem = listaTipoIdentificacionSelectItem;
    }

    public List<SelectItem> getListaTipoIdentificacionSelectItem() {
        return listaTipoIdentificacionSelectItem;
    }

    public void setIdentificacionSeleccionada(String identificacionSeleccionada) {
        this.identificacionSeleccionada = identificacionSeleccionada;
    }

    public String getIdentificacionSeleccionada() {
        return identificacionSeleccionada;
    }


    public void setNumeroIdentificacionSeleccionada(String numeroIdentificacionSeleccionada) {
        this.numeroIdentificacionSeleccionada = numeroIdentificacionSeleccionada;
    }

    public String getNumeroIdentificacionSeleccionada() {
        return numeroIdentificacionSeleccionada;
    }

    //Eventos
    
    public void verValorSeleccionado(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        valueChangeEvent.getNewValue();
        System.out.println("Nuevo valor : "+valueChangeEvent.getNewValue());
        System.out.println("Viejo valor : "+valueChangeEvent.getOldValue());
    }

    //Tratamientos de la informacion
    

    private void cargaInicial() {
        
        this.listaTipoIdentificacionSelectItem = new ArrayList<SelectItem>();        

        String msj = TipoIdentificacionFacade.getTipoIdentificacionWs(this.listaTipoIdentificacionSelectItem);
        LoggerUtil.sendLogMessage("Servicio",msj);
             
        
        //Ejempo cableado de las lista de personas
        this.listaPersona = new ArrayList<Persona>();
        popularTabla();
        
    }

    public void enviarSeleccion(ActionEvent actionEvent) {
        // Add event code here...
        JSFUtil.addFacesInformationMessage("id :"+this.identificacionSeleccionada+" numero :"+this.numeroIdentificacionSeleccionada);
        
        String descripcionTipoIdentificacionSeleccionada = "";
        boolean flagCoincidence = false;
        
        for (SelectItem item : this.listaTipoIdentificacionSelectItem){
            if(item.getValue().equals(Integer.parseInt(this.identificacionSeleccionada))){
                descripcionTipoIdentificacionSeleccionada = item.getLabel();
            }
        }
        
        for (Persona persona : this.listaPersona){
            if(persona.getNumeroidentificacion() == Integer.parseInt(this.numeroIdentificacionSeleccionada) && persona.getDescripcionTipoIdentificacion().equals(descripcionTipoIdentificacionSeleccionada)){
                JSFUtil.addFacesInformationMessage("La persona se encuentra en BD");
                this.listaPersona.clear();
                this.listaPersona.add(persona);
                JSFUtil.refreshComponent(JSFUtil.findComponentInRoot("t1"));
                flagCoincidence = true;
            }
        }
        
        if(flagCoincidence != true){
            JSFUtil.addFacesErrorMessage("La persona no se encuentra en base de datos");
        }
  
    }

    private void popularTabla() {
        //Manejo d elas fecha cableadas
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        try {
            
            this.listaPersona.add(new Persona("Jorge", "Enrique", 123456, 123456, "CEDULA DE CIUDADANIA", "Medina", "Bencomo", 23, formateador.parse("23/04/2017"), 2456.43, Boolean.TRUE));
            this.listaPersona.add(new Persona("Isabel", "Cristina", 123456, 123456, "PASAPORTE", "Yepez", "Bencomo", 54, formateador.parse("12/11/2017"), 342.43, Boolean.FALSE));
            this.listaPersona.add(new Persona("Guillermo", "Jose", 123456, 123456, "TARJETA IDENTIDAD", "Gomez", "Padron", 29, formateador.parse("23/02/2018"), 8907.43, Boolean.TRUE));
            
        } catch (ParseException e) {
            LoggerUtil.sendLogMessage("Fecha",e.getMessage());
        }

    }
}
