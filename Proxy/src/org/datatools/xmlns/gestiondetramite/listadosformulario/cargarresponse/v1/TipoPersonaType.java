
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoPersonaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TipoPersonaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipoPersonaType", propOrder = { "idTipo", "nombreTipo" })
public class TipoPersonaType {

    protected String idTipo;
    protected String nombreTipo;

    /**
     * Gets the value of the idTipo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdTipo() {
        return idTipo;
    }

    /**
     * Sets the value of the idTipo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdTipo(String value) {
        this.idTipo = value;
    }

    /**
     * Gets the value of the nombreTipo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNombreTipo() {
        return nombreTipo;
    }

    /**
     * Sets the value of the nombreTipo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNombreTipo(String value) {
        this.nombreTipo = value;
    }

}
