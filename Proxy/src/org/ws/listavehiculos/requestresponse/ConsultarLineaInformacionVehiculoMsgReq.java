
package org.ws.listavehiculos.requestresponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMarca" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "idMarca" })
@XmlRootElement(name = "ConsultarLineaInformacionVehiculoMsgReq",
                namespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0")
public class ConsultarLineaInformacionVehiculoMsgReq {

    @XmlElement(namespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0")
    protected int idMarca;

    /**
     * Gets the value of the idMarca property.
     *
     */
    public int getIdMarca() {
        return idMarca;
    }

    /**
     * Sets the value of the idMarca property.
     *
     */
    public void setIdMarca(int value) {
        this.idMarca = value;
    }

}
