
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TipoIdentificacion" type="{http://xmlns.datatools.org/GestionDeTramite/ListadosFormulario/CargarResponse/V1.0}TipoIdentificacionType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "tipoIdentificacion" })
@XmlRootElement(name = "GestionDeTramite_ConsultarTiposIdentificacionMsgResp")
public class GestionDeTramiteConsultarTiposIdentificacionMsgResp {

    @XmlElement(name = "TipoIdentificacion", required = true)
    protected List<TipoIdentificacionType> tipoIdentificacion;

    /**
     * Gets the value of the tipoIdentificacion property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoIdentificacion property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoIdentificacion().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoIdentificacionType }
     *
     *
     */
    public List<TipoIdentificacionType> getTipoIdentificacion() {
        if (tipoIdentificacion == null) {
            tipoIdentificacion = new ArrayList<TipoIdentificacionType>();
        }
        return this.tipoIdentificacion;
    }
    
    

}
