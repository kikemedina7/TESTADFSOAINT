
package org.datatools.xmlns.gestiondetramite.informacionvehiculo.obtenerresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehiculoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="VehiculoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idVehiculo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="alertasLegales" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaseVehiculo" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="Linea" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="Marca" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="Modelo" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="Ciudad" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="ModalidadTransporte" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}GenericItemType" minOccurs="0"/&gt;
 *         &lt;element name="Placa" type="{http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/ObtenerResponse/V1.0}PlacaType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehiculoType", propOrder = {
         "idVehiculo", "alertasLegales", "claseVehiculo", "linea", "marca", "modelo", "ciudad", "modalidadTransporte",
         "placa"
    })
public class VehiculoType {

    protected Integer idVehiculo;
    protected String alertasLegales;
    @XmlElement(name = "ClaseVehiculo")
    protected GenericItemType claseVehiculo;
    @XmlElement(name = "Linea")
    protected GenericItemType linea;
    @XmlElement(name = "Marca")
    protected GenericItemType marca;
    @XmlElement(name = "Modelo")
    protected GenericItemType modelo;
    @XmlElement(name = "Ciudad")
    protected GenericItemType ciudad;
    @XmlElement(name = "ModalidadTransporte")
    protected GenericItemType modalidadTransporte;
    @XmlElement(name = "Placa", required = true)
    protected PlacaType placa;

    /**
     * Gets the value of the idVehiculo property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    /**
     * Sets the value of the idVehiculo property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setIdVehiculo(Integer value) {
        this.idVehiculo = value;
    }

    /**
     * Gets the value of the alertasLegales property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAlertasLegales() {
        return alertasLegales;
    }

    /**
     * Sets the value of the alertasLegales property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAlertasLegales(String value) {
        this.alertasLegales = value;
    }

    /**
     * Gets the value of the claseVehiculo property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getClaseVehiculo() {
        return claseVehiculo;
    }

    /**
     * Sets the value of the claseVehiculo property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setClaseVehiculo(GenericItemType value) {
        this.claseVehiculo = value;
    }

    /**
     * Gets the value of the linea property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getLinea() {
        return linea;
    }

    /**
     * Sets the value of the linea property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setLinea(GenericItemType value) {
        this.linea = value;
    }

    /**
     * Gets the value of the marca property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getMarca() {
        return marca;
    }

    /**
     * Sets the value of the marca property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setMarca(GenericItemType value) {
        this.marca = value;
    }

    /**
     * Gets the value of the modelo property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setModelo(GenericItemType value) {
        this.modelo = value;
    }

    /**
     * Gets the value of the ciudad property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getCiudad() {
        return ciudad;
    }

    /**
     * Sets the value of the ciudad property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setCiudad(GenericItemType value) {
        this.ciudad = value;
    }

    /**
     * Gets the value of the modalidadTransporte property.
     *
     * @return
     *     possible object is
     *     {@link GenericItemType }
     *
     */
    public GenericItemType getModalidadTransporte() {
        return modalidadTransporte;
    }

    /**
     * Sets the value of the modalidadTransporte property.
     *
     * @param value
     *     allowed object is
     *     {@link GenericItemType }
     *
     */
    public void setModalidadTransporte(GenericItemType value) {
        this.modalidadTransporte = value;
    }

    /**
     * Gets the value of the placa property.
     *
     * @return
     *     possible object is
     *     {@link PlacaType }
     *
     */
    public PlacaType getPlaca() {
        return placa;
    }

    /**
     * Sets the value of the placa property.
     *
     * @param value
     *     allowed object is
     *     {@link PlacaType }
     *
     */
    public void setPlaca(PlacaType value) {
        this.placa = value;
    }

}
