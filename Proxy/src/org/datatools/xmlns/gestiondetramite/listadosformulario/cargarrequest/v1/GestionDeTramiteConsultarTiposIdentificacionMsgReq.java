
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarrequest.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "idUsuario" })
@XmlRootElement(name = "GestionDeTramite_ConsultarTiposIdentificacionMsgReq")
public class GestionDeTramiteConsultarTiposIdentificacionMsgReq {

    @XmlElement(required = true)
    protected String idUsuario;

    /**
     * Gets the value of the idUsuario property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Sets the value of the idUsuario property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

}
