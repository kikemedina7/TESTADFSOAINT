
package org.ws.listavehiculos;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.ws.listavehiculos.requestresponse.ConsultarClaseInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarClaseInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarLineaInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarLineaInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarMarcaInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarMarcaInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarModalidadTransporteInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarModalidadTransporteInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ConsultarModeloInformacionVehiculoMsgReq;
import org.ws.listavehiculos.requestresponse.ConsultarModeloInformacionVehiculoMsgResp;
import org.ws.listavehiculos.requestresponse.ObjectFactory;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150616.1732
 * Generated source version: 2.2
 *
 */
@WebService(name = "InformacionVehiculosPt",
            targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/V1.0")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public interface InformacionVehiculosPt {


    /**
     *
     * @param consultarModalidadTransporteInformacionVehiculosReqParam
     * @return
     *     returns org.ws.listavehiculos.requestresponse.ConsultarModalidadTransporteInformacionVehiculoMsgResp
     */
    @WebMethod(operationName = "ConsultarModalidadTransporteInformacionVehiculosOp",
               action =
               "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/ConsultarModalidadTransporte/Op")
    @WebResult(name = "ConsultarModalidadTransporteInformacionVehiculoMsgResp",
               targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Response/V1.0",
               partName = "ConsultarModalidadTransporteInformacionVehiculosRespParam")
    public ConsultarModalidadTransporteInformacionVehiculoMsgResp consultarModalidadTransporteInformacionVehiculosOp(@WebParam(name =
                                                                                                                               "ConsultarModalidadTransporteInformacionVehiculoMsgReq",
                                                                                                                               targetNamespace =
                                                                                                                               "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0",
                                                                                                                               partName =
                                                                                                                               "ConsultarModalidadTransporteInformacionVehiculosReqParam")
                                                                                                                     ConsultarModalidadTransporteInformacionVehiculoMsgReq consultarModalidadTransporteInformacionVehiculosReqParam);

    /**
     *
     * @param consultarClaseInformacionVehiculosReqParam
     * @return
     *     returns org.ws.listavehiculos.requestresponse.ConsultarClaseInformacionVehiculoMsgResp
     */
    @WebMethod(operationName = "ConsultarClaseInformacionVehiculosOp",
               action = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/ConsultarClase/Op")
    @WebResult(name = "ConsultarClaseInformacionVehiculoMsgResp",
               targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Response/V1.0",
               partName = "ConsultarClaseInformacionVehiculosRespParam")
    public ConsultarClaseInformacionVehiculoMsgResp consultarClaseInformacionVehiculosOp(@WebParam(name =
                                                                                                   "ConsultarClaseInformacionVehiculoMsgReq",
                                                                                                   targetNamespace =
                                                                                                   "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0",
                                                                                                   partName =
                                                                                                   "ConsultarClaseInformacionVehiculosReqParam")
                                                                                         ConsultarClaseInformacionVehiculoMsgReq consultarClaseInformacionVehiculosReqParam);

    /**
     *
     * @param consultarMarcaInformacionVehiculosReqParam
     * @return
     *     returns org.ws.listavehiculos.requestresponse.ConsultarMarcaInformacionVehiculoMsgResp
     */
    @WebMethod(operationName = "ConsultarMarcaInformacionVehiculosOp",
               action = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/ConsultarMarca/Op")
    @WebResult(name = "ConsultarMarcaInformacionVehiculoMsgResp",
               targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Response/V1.0",
               partName = "ConsultarMarcaInformacionVehiculosRespParam")
    public ConsultarMarcaInformacionVehiculoMsgResp consultarMarcaInformacionVehiculosOp(@WebParam(name =
                                                                                                   "ConsultarMarcaInformacionVehiculoMsgReq",
                                                                                                   targetNamespace =
                                                                                                   "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0",
                                                                                                   partName =
                                                                                                   "ConsultarMarcaInformacionVehiculosReqParam")
                                                                                         ConsultarMarcaInformacionVehiculoMsgReq consultarMarcaInformacionVehiculosReqParam);

    /**
     *
     * @param consultarLineaInformacionVehiculosReqParam
     * @return
     *     returns org.ws.listavehiculos.requestresponse.ConsultarLineaInformacionVehiculoMsgResp
     */
    @WebMethod(operationName = "ConsultarLineaInformacionVehiculosOp",
               action = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/ConsultarLinea/Op")
    @WebResult(name = "ConsultarLineaInformacionVehiculoMsgResp",
               targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Response/V1.0",
               partName = "ConsultarLineaInformacionVehiculosRespParam")
    public ConsultarLineaInformacionVehiculoMsgResp consultarLineaInformacionVehiculosOp(@WebParam(name =
                                                                                                   "ConsultarLineaInformacionVehiculoMsgReq",
                                                                                                   targetNamespace =
                                                                                                   "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0",
                                                                                                   partName =
                                                                                                   "ConsultarLineaInformacionVehiculosReqParam")
                                                                                         ConsultarLineaInformacionVehiculoMsgReq consultarLineaInformacionVehiculosReqParam);

    /**
     *
     * @param consultarModeloInformacionVehiculosReqParam
     * @return
     *     returns org.ws.listavehiculos.requestresponse.ConsultarModeloInformacionVehiculoMsgResp
     */
    @WebMethod(operationName = "ConsultarModeloInformacionVehiculosOp",
               action = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculos/ConsultarModelo/Op")
    @WebResult(name = "ConsultarModeloInformacionVehiculoMsgResp",
               targetNamespace = "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Response/V1.0",
               partName = "ConsultarModeloInformacionVehiculosRespParam")
    public ConsultarModeloInformacionVehiculoMsgResp consultarModeloInformacionVehiculosOp(@WebParam(name =
                                                                                                     "ConsultarModeloInformacionVehiculoMsgReq",
                                                                                                     targetNamespace =
                                                                                                     "http://xmlns.datatools.org/GestionDeTramite/InformacionVehiculo/Request/V1.0",
                                                                                                     partName =
                                                                                                     "ConsultarModeloInformacionVehiculosReqParam")
                                                                                           ConsultarModeloInformacionVehiculoMsgReq consultarModeloInformacionVehiculosReqParam);

}
