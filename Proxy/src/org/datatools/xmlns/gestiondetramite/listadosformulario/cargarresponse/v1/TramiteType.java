
package org.datatools.xmlns.gestiondetramite.listadosformulario.cargarresponse.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TramiteType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TramiteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TramiteType", propOrder = { "idTramite", "nombreTramite" })
public class TramiteType {

    protected String idTramite;
    protected String nombreTramite;

    /**
     * Gets the value of the idTramite property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdTramite() {
        return idTramite;
    }

    /**
     * Sets the value of the idTramite property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdTramite(String value) {
        this.idTramite = value;
    }

    /**
     * Gets the value of the nombreTramite property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNombreTramite() {
        return nombreTramite;
    }

    /**
     * Sets the value of the nombreTramite property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNombreTramite(String value) {
        this.nombreTramite = value;
    }

}
